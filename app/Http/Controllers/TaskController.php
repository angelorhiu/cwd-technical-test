<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Task;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::orderBy('created_at', 'desc')->paginate(5);


        return view('tasks.index')->with('tasks', $tasks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // $activity = 'todo';
        // if($activity == 'notes'){
        //     return view('tasks.notes');
        // }
        // else{
        //     return view('tasks.create');
        // }

        // $task = new Task;

        // $task->name = $request->input('name');
        // $task->description = $request->input('description');
        // $task->status = $request->input('status');

        // $task->save();

        // return response()->json($task);


        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255|min:2',
            'description' => 'required|string|max:10000|min:10',
        ]);

        $task = new Task;

        $task->name = $request->name;
        $task->description = $request->description;
        $task->status = $request->status;

        $task->save();

        Session::flash('success', 'Created Task Successfully');

        return redirect()->route('task.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);
        return view('tasks.edit')->with('task', $task);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255|min:2',
            'description' => 'required|string|max:10000|min:10',
        ]);

        $task = Task::find($id);

        $task->name = $request->name;
        $task->description = $request->description;
        $task->status = $request->status;

        $task->save();

        Session::flash('success', 'Updated Task Successfully');

        return redirect()->route('task.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);

        $task->delete();

        Session::flash('success', 'Deleted Task Successfully');

        return redirect()->route('task.index');
    }

    public function note()
    {
        return view('tasks.notes');
    }
}
