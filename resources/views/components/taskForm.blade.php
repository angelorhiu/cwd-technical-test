	{{ Form::label('name', 'Task Name', ['class' => 'control-label'])}}
	{{ Form::text('name', null, ['class' => 'form-control form-control-lg', 'placeholder' => 'Task Description'])}}

	{{ Form::label('description', 'Task Description', ['class' => 'control-label mt-3'])}}
	{{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Task Description'])}}

	<div class="btn-group btn-group-toggle mt-3" data-toggle="buttons">
	  <label class="btn btn-secondary active">
	    <input type="radio" name="status" value="Completed" checked> Task Completed
	  </label>
	  <label class="btn btn-secondary">
	    <input type="radio" name="status" value="Not Completed"> Task Not Completed
	  </label>
	</div>
	<div class="row justify-content-center mt-3">
		<div class="col-sm-4">
			<a href="{{ route('task.index')}}" class="btn btn-block btn-success">Back</a>
		</div>
		<div class="col-sm-4">
			<input class="btn btn-block btn-success" type="submit" name="createTask" value="Save Task"/>
		</div>
	</div>
