	{{ Form::label('name', 'Note Title', ['class' => 'control-label'])}}
	{{ Form::text('name', null, ['class' => 'form-control form-control-lg', 'placeholder' => 'Task Description'])}}

	{{ Form::label('description', 'Note Description', ['class' => 'control-label mt-3'])}}
	{{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Task Description'])}}

	<div class="row justify-content-center mt-3">
		<div class="col-sm-4">
			<a href="{{ route('task.index')}}" class="btn btn-block btn-success">Back</a>
		</div>
		<div class="col-sm-4">
			<input class="btn btn-block btn-success" type="submit" name="createNote" value="Save Note"/>
		</div>
	</div>
