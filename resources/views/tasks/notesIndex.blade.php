@extends('layouts.main')

@section('title', 'My Notes List')

@section('content')

	<div class="row mb-3">
		<div class="col-sm-5">
			<a href="{{ route('task.create')}}" class="btn btn-success">Switch to To Do Lists</a>
		</div>
		<div class="col-sm-3">
			<h2 class="justify-content-center">My Notes List</h2>
		</div>
		<div class="col-sm-4 text-right">
			<a href="{{ route('task.create')}}" class="btn btn-success">Create New Notes</a>
		</div>
	</div>
		
		<div class="row wrap">
			<div class="col-sm-12 mt-3">
				<div class="card text-white bg-info mb-3" style="max-width: 18rem;">
				  <div class="card-header">Header</div>
				  <div class="card-body">
				    <h5 class="card-title">Info card title</h5>
				    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
				  </div>
				</div>
			</div>
		</div>

	<div class="row justify-content-center">
		<div class="col-sm-6 text-center">
			{{ $tasks->links() }}
		</div>
	</div>
@endsection