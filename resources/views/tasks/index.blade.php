@extends('layouts.main')

@section('title', 'My To Do List')

@section('content')

	<div class="row mb-3">
		<div class="col-sm-5">
			<!-- <a href="{{ route('task.create')}}" class="btn btn-success">Switch to Notes Lists</a> -->
		</div>
		<div class="col-sm-3">
			<h2 class="justify-content-center">My To Do List</h2>
		</div>
		<div class="col-sm-4 text-right">
			<a href="{{ route('task.create')}}" class="btn btn-success">Create New Task</a>
		</div>
	</div>

	@if($tasks->count() == 0)
		<h3>No current task/s listed.</h3>
	@else

	@foreach($tasks as $task)
		<div class="row wrap">
			<div class="col-sm-12 mt-3">
				<div class="card border-secondary mb-3">
				  <div class="card-header"><h3>{{ $task->name }}</h3></div>
				  <div class="card-body text-dark">
				    <p class="card-text">{{ $task->description }}</p>
				    {!! Form::open(['route' => ['task.destroy', $task->id], 'method' => 'DELETE'])!!}
					    <a href="{{ route('task.edit', $task->id)}}" class="btn btn-sm btn-primary">Edit Task</a>
					    <button type="submit" class="btn btn-sm btn-danger">Remove Task</button>
				    {!! Form::close() !!}
				  </div>
				<div class="card-footer">
			      <small class="text-muted">Status: {{ $task->status }}</small>
			    </div>
				</div>
			</div>
		</div>
	@endforeach
	@endif

	<div class="row justify-content-center">
		<div class="col-sm-6 text-center">
			{{ $tasks->links() }}
		</div>
	</div>
@endsection