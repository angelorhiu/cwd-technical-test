@extends('layouts.main')

@section('title', 'Create Note')

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<h1>Create Note</h1>
			{!!	Form::open(['route' => 'task.store', 'method' => 'POST']) !!}
				@component('components.notesForm')
				@endcomponent
			{!! Form::close() !!}
		</div>
	</div>
	

@endsection